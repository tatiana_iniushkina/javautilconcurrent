package ru.demo.callabledemo;

import java.util.concurrent.Callable;

class Factorial implements Callable<Integer> {
    private int stop;

    Factorial(int stop) {
        this.stop = stop;
    }

    @Override
    public Integer call() {
        int fact = 1;
        for (int i = 2; i <= stop; i++) {
            fact *= i;
        }
        return fact;
    }
}
