package ru.demo.callabledemo;

import java.util.concurrent.Callable;

public class Sum implements Callable<Integer> {
    private int stop;

    Sum(int stop) {
        this.stop = stop;
    }

    public Integer call() {
        int sum = 0;
        for (int i = 1; i <= stop; i++) {
            sum += i;
        }
        return sum;
    }
}
