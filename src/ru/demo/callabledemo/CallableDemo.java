package ru.demo.callabledemo;

import java.util.concurrent.*;

public class CallableDemo {
    public static void main(String[] args) {
        ExecutorService executorService = Executors.newFixedThreadPool(3);
        Future<Integer> sum;
        Future<Double> hypotenuse;
        Future<Integer> factorial;
        System.out.println("Start");
        sum = executorService.submit(new Sum(10));
        hypotenuse = executorService.submit(new Hypotenuse(3, 4));
        factorial = executorService.submit(new Factorial(5));
        try {
            System.out.println(sum.get());
            System.out.println(hypotenuse.get());
            System.out.println(factorial.get());
        } catch (InterruptedException | ExecutionException e) {
            System.out.println(e);
        }
        executorService.shutdown();
        System.out.println("Finish");
    }
}

