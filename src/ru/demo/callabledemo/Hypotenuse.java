package ru.demo.callabledemo;

import java.util.concurrent.Callable;

class Hypotenuse implements Callable<Double> {
    private double side1;
    private double side2;

    Hypotenuse(double side1, double side2) {
        this.side1 = side1;
        this.side2 = side2;
    }

    @Override
    public Double call() {
        return Math.sqrt(side1 * side1 + side2 * side2);
    }
}
