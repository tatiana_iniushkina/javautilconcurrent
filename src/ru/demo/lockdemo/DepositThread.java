package ru.demo.lockdemo;

class DepositThread extends Thread {

    private final Account account;

    DepositThread(Account account) {
        this.account = account;
    }

    @Override
    public void run() {
        for (int i = 0; i < 60_000_000; ++i) {
            account.deposit((long) (Math.random() * 1_000L));
        }
    }
}
