package ru.demo.lockdemo;

public class LockDemo {
    public static void main(String[] args) throws Exception {
        Account account = new Account(1_000L);

        new DepositThread(account).start();

        account.waitAndWithdraw(50_000_000);
        System.out.println("waitAndWithdraw finished, end balance = " + account.getBalance());

        account.waitAndWithdraw(5_000_000);
        System.out.println("waitAndWithdraw finished, end balance = " + account.getBalance());
    }
}
