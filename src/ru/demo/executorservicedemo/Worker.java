package ru.demo.executorservicedemo;

import java.util.concurrent.Callable;

class Worker implements Callable<String> {

    private final String name;

    Worker(String name) {
        this.name = name;
    }

    public String call() throws Exception {
        long sleepTime = (long) (Math.random() * 10000L);
        System.out.println(name + " started, going to sleep for " + sleepTime + "ms");
        Thread.sleep(sleepTime);
        System.out.println(name + " finished");
        return name;
    }
}
