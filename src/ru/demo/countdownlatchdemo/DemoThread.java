package ru.demo.countdownlatchdemo;

import java.util.concurrent.CountDownLatch;

public class DemoThread extends Thread {

    private final CountDownLatch latch;

    DemoThread(CountDownLatch latch) {
        this.latch = latch;
    }

    @Override
    public void run() {
        try {
            runUnsafe();
        } catch (InterruptedException e) {
            System.out.println(getName() + " interrupted");
        }
    }

    private void runUnsafe() throws InterruptedException {
        // initialization phase
        Thread.sleep((long) (Math.random() * 10000L));

        System.out.println(getName() + " finished initialization");

        latch.countDown();
        latch.await();

        System.out.println(getName() + " entered main phase");

        // main phase
        Thread.sleep((long) (Math.random() * 10000L));
    }
}
