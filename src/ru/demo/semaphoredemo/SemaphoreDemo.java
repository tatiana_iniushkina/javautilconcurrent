package ru.demo.semaphoredemo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class SemaphoreDemo {
    public static void main(String[] args) throws Exception {
        Semaphore semaphore = new Semaphore(2);

        List<Thread> threads = new ArrayList<>();
        for (int i = 0; i < 10; ++i) {
            DemoThread thread = new DemoThread(semaphore);
            threads.add(thread);
            thread.start();
        }

        Thread.sleep(20000);

        for (Thread thread : threads) {
            thread.interrupt();
        }
    }
}
